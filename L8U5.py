def index_body(height, weight):
    return weight / (height*0.01)**2

while True:
    print('\n1. Програма обчислення індексу тіла.')
    choice = str(input('Введіть дію on або off: '))
    if choice == 'off':
        break
    if choice == 'on':
        height = float(input('Введіть свій зріст в сантиметрах: '))
        weight = float(input('Введіть свою вагу: '))
    print('\nІндекс вашого тіла дорівнює: ', index_body(height, weight).__round__(2))
    if index_body(height, weight) < 18.5:
        print('Недостатня вага')
    elif index_body(height, weight) < 24.9:
        print('Маса тіла в нормі')
    else:
        print('Слідкуйте за фігурою, ваш індекс перевищує норму')
